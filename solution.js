const employees = require('./arrays-jobs.js');

// 1. Find all Web Developers
function getWebDevelopers() {
    let result = [];

    result = employees.filter(function (employee) {
        if (employee.job.includes("Web Developer")) {
            return employee;
        }
    });

    return result;
}

// console.log(JSON.stringify(getWebDevelopers()));

// 2. Convert all the salary values into proper numbers instead of strings
function convertSalary() {
    let result = [];

    result = employees.map(function (employee) {
        // ignore first char as it is a currency symbol
        return Number(employee.salary.slice(1));
    });

    return result;
}

// console.log(convertSalary());

// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
function correctedSalary() {
    // not modifying the original array
    let result = employees;

    employees.forEach(function (employee, index) {
        let salary = Number(employee.salary.slice(1));
        result[index].correctedSalary = "$" + String(salary * 10000);
    }, {});
    
    return result;
}

console.log(correctedSalary());

// 4. Find the sum of all salaries 
function totalSalary() {
    let sum;

    sum = employees.reduce(function (sum, employee) {
        let salary = Number(employee.salary.slice(1));
        return sum + salary;
    }, 0);

    return sum;
}

// console.log(totalSalary());

// 5. Find the sum of all salaries based on country using only HOF method
function salaryByCountry() {
    let result;

    result = employees.reduce(function (salaryBasedOnCountry, employee) {
        // removing currency symbol from salary
        let salary = Number(employee.salary.slice(1));

        if (salaryBasedOnCountry.hasOwnProperty(employee.location) === false) {
            salaryBasedOnCountry[employee.location] = salary;
        } else {
            salaryBasedOnCountry[employee.location] += salary;
        }

        return salaryBasedOnCountry;
    }, {});

    return result;
}

// console.log(salaryByCountry());

// 6. Find the average salary of based on country using only HOF method
function averageSalaryByCountry() {

    let sumOfSalaryByCountry = salaryByCountry();

    let numberOfSalaryInEachCountry = employees.reduce(function(numberOfSalaryInEachCountry, employee) {
        if (numberOfSalaryInEachCountry.hasOwnProperty(employee.location) === false) {
            numberOfSalaryInEachCountry[employee.location] = 1;
        } else {
            numberOfSalaryInEachCountry[employee.location]++;
        }

        return numberOfSalaryInEachCountry;
    }, {});

    let averageSalaryInEachCountry = Object.keys(sumOfSalaryByCountry).map(function( location) {
        let temp = {};
        temp[location] = sumOfSalaryByCountry[location] / numberOfSalaryInEachCountry[location];

        return temp;
    });


    return averageSalaryInEachCountry;
}

console.log(averageSalaryByCountry());